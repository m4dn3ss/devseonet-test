<?php

class M4dn3ssAutoloader
{

    public static function autoload($className)
    {
        $namespaces = explode('\\', $className);
        if(isset($namespaces[0]) && $namespaces[0] === 'm4dn3ss') {
            unset($namespaces[0]);
        }
        require_once self::buildPath($namespaces) . '.php';
    }


    /**
     * Builds path for include file from array.
     * Sanitizes this path before returns.
     * @param $array
     * @return null|string
     */
    private static function buildPath($array)
    {
        if(!is_array($array) || is_array($array) && empty($array))
            return null;
        $path = __DIR__;
        foreach ($array as $element) {
            $fragment = self::validateName($element);
            if(!empty($fragment)) {
                $path .= DS . $fragment;
            }
        }
        return $path;
    }

    /**
     * Validates string by given regular expression.
     * Returns given string if it was validated, or null if it wasn't.
     * @param $string
     * @param string $regExp
     * @return null
     */
    private static function validateName($string, $regExp = '/\w+/') {
        if(preg_match($regExp, $string)) {
            return $string;
        }
        return null;
    }

}

spl_autoload_register('M4dn3ssAutoloader::autoload');