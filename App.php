<?php
namespace m4dn3ss;

use m4dn3ss\framework\Controller;
use m4dn3ss\framework\Database;
use m4dn3ss\framework\Request;
use m4dn3ss\framework\Config;
use m4dn3ss\framework\Router;
use m4dn3ss\framework\View;

/**
 * Class App
 * @package m4dn3ss
 * @author Viacheslav Zhabonos - vyacheslav0310@gmail.com
 *
 * @property Request $request
 * @property Config $config
 * @property Database $db
 *
 */

class App
{
    private static $request = null, $config = null, $db = null;

    public function __construct()
    {
        require_once __DIR__ . DS . 'routes' . DS . 'main.php';
    }

    /**
     * @return Request
     */
    public static function request()
    {
        if(self::$request === null)
            self::$request = new Request();

        return self::$request;
    }

    /**
     * @return Config
     */
    public static function config()
    {
        if(self::$config === null)
            self::$config = new Config();

        return self::$config;
    }

    /**
     * @return Database
     */
    public static function db()
    {
        if(self::$db === null) {
            try {
                self::$db = new Database(self::config()->getParam('db'));
            }
            catch(\Exception $e) {
                exit($e->getMessage());
            }
        }
        return self::$db;
    }

    /**
     * Main function
     * @throws \Exception
     */

    public function run()
    {
        $action = Router::resolve();
        if ($action && isset($action['function']) && is_callable($action['function'])) {
            $parameters = $action['parameters'] ?? array();
            call_user_func_array($action['function'], $parameters);
        }
    }
}