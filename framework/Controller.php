<?php

namespace m4dn3ss\framework;


use m4dn3ss\App;

class Controller
{

    /**
     * @param $file
     * @param null $data
     * @throws \Exception
     */
    public function render($file, $data = null)
    {
        $view = new View($file, App::config()->getParam('views:viewsDirectory'), App::config()->getParam('views:layoutFile'), App::config()->getParam('publicFolderOutside'));
        echo $view->render($data);
    }
}