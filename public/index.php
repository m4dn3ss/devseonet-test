<?php

// show errors?
ini_set('display_errors', 1);

// start session
session_start();

// define simpler directory separator constant
if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

// function for easier debugging
function m_dump($var, $exit = false)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';

    if ($exit)
        exit();
}

// autoloader file
require_once '..' . DS . 'autoload.php';

// Creating new application instance
(new \m4dn3ss\App())->run();