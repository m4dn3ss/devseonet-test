<?php

return array(
    'db' => include('db.php'),
    'views' => [
        'layoutFile' => 'layout',
        'viewsDirectory' => 'views',
    ],
    'controllers' => [
        'namespace' => 'm4dn3ss\\controllers',
        'errorController' => 'ErrorController'
    ],
    'publicFolderOutside' => true
);