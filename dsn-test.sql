-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Квт 15 2019 р., 16:24
-- Версія сервера: 10.3.13-MariaDB
-- Версія PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `dsn-test`
--

-- --------------------------------------------------------

--
-- Структура таблиці `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `options`
--

INSERT INTO `options` (`id`, `name`) VALUES
(1, 'Brand'),
(2, 'Screen resolution'),
(3, 'Screen size');

-- --------------------------------------------------------

--
-- Структура таблиці `option_values`
--

CREATE TABLE `option_values` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `option_values`
--

INSERT INTO `option_values` (`id`, `name`, `option_id`) VALUES
(1, 'Apple', 1),
(2, 'Samsung', 1),
(3, '1334x750', 2),
(4, '1136 x 640 ', 2),
(5, '1280x720', 2),
(6, '1480x720', 2),
(7, '5.6\'', 3),
(8, '5.5\'', 3),
(9, '4.7\'', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`id`, `name`, `description`) VALUES
(1, 'Apple Iphone 5', 'Apple Iphone 5'),
(2, 'Apple Iphone 6', 'Apple Iphone 6'),
(3, 'Samsung J7', 'Samsung J7'),
(4, 'Samsung J6', 'Samsung J6');

-- --------------------------------------------------------

--
-- Структура таблиці `products_option_values`
--

CREATE TABLE `products_option_values` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `products_option_values`
--

INSERT INTO `products_option_values` (`id`, `product_id`, `option_value_id`) VALUES
(1, 1, 9),
(2, 2, 9),
(3, 1, 1),
(4, 2, 1),
(5, 1, 4),
(6, 2, 3),
(7, 3, 5),
(8, 4, 6),
(9, 4, 2),
(10, 3, 2);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `option_values`
--
ALTER TABLE `option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option` (`option_id`);

--
-- Індекси таблиці `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `products_option_values`
--
ALTER TABLE `products_option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product` (`product_id`),
  ADD KEY `option_value` (`option_value_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблиці `option_values`
--
ALTER TABLE `option_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблиці `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблиці `products_option_values`
--
ALTER TABLE `products_option_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `option_values`
--
ALTER TABLE `option_values`
  ADD CONSTRAINT `option` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_option_values`
--
ALTER TABLE `products_option_values`
  ADD CONSTRAINT `option_value` FOREIGN KEY (`option_value_id`) REFERENCES `option_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
