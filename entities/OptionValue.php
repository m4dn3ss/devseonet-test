<?php

namespace m4dn3ss\entities;

use m4dn3ss\App;
use m4dn3ss\framework\Entity;

/**
 * Class OptionValue
 *
 * @inheritdoc
 * @package m4dn3ss\entities
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 *
 */
class OptionValue extends Entity
{
    protected static $tableName = 'option_values';

    /**
     * @param array $productIds
     * @return array
     */
    public static function getOptions($productIds = array())
    {
        $options = App::db()->query(
            "SELECT " .
            self::getTableName() . ".name, " .
            self::getTableName() . ".id as value_id, " .
            Option::getTableName() . ".name as option_name, (SELECT COUNT(*) FROM " .
            ProductOptionValue::getTableName() . " WHERE " .
            ProductOptionValue::getTableName() . ".option_value_id = " .
            self::getTableName() . ".id" . (!empty($productIds) ? " AND " .
            ProductOptionValue::getTableName() . ".product_id IN (:productIds)" : '') . ") as products_count FROM " .
            self::getTableName() . " LEFT JOIN " .
            Option::getTableName() . " ON " .
            self::getTableName() . ".option_id = " .
            Option::getTableName() . ".id" .  ( !empty($productIds) ? " WHERE " .
            self::getTableName() . ".id IN (SELECT " .
            ProductOptionValue::getTableName() . ".option_value_id FROM " .
            ProductOptionValue::getTableName() . " WHERE " .
            ProductOptionValue::getTableName() . ".product_id IN (:productIds))" : "") . " HAVING products_count > 0",
            array(':productIds' => $productIds)
        );
        $preparedOptions = array();
        foreach ($options as $option) {
            $preparedOptions[$option['option_name']][] = $option;
        }

        return $preparedOptions;
    }
}