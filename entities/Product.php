<?php

namespace m4dn3ss\entities;

use m4dn3ss\App;
use m4dn3ss\framework\Entity;

/**
 * Class Product
 *
 * @inheritdoc
 * @package m4dn3ss\entities
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 */
class Product extends Entity
{
    protected static $tableName = 'products';

    /**
     * @param $filter
     * @return array
     */
    public static function filterProducts($filter)
    {
        $filterBinding = array();
        $filterConditions = '';

        $filterQueryTemplate = "SELECT * FROM "
            . ProductOptionValue::getTableName() . " WHERE "
            . ProductOptionValue::getTableName() . ".product_id = t1.id AND "
            . ProductOptionValue::getTableName() . ".option_value_id = {value}";

        if ($filter) {
            foreach ($filter as $key => $filterValue) {
                $bindKey = ':filter' . $key;
                $filterBinding[$bindKey] = $filterValue;
                $condition = str_replace('{value}', $bindKey, $filterQueryTemplate);
                if (empty($filterConditions)) {
                    $filterConditions = $condition;
                } else {
                    $filterConditions = str_replace('{query}', $condition, $filterConditions);
                }
                $filterConditions .= (isset($filter[$key + 1]) ? " AND EXISTS({query})" : "");
            }
        }

        return App::db()->query(
            "SELECT t1.* FROM " . self::getTableName() . " AS t1" .
            (!empty($filterConditions) ? " WHERE EXISTS(" . $filterConditions . ")": ""),
            $filterBinding
        );

    }
}