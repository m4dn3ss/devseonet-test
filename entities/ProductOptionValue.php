<?php

namespace m4dn3ss\entities;

use m4dn3ss\framework\Entity;

/**
 * Class ProductOptionValue
 *
 * @inheritdoc
 * @package m4dn3ss\entities
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 *
 */
class ProductOptionValue extends Entity
{
    protected static $tableName = 'products_option_values';
}