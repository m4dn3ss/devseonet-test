<?php

namespace m4dn3ss\entities;

use m4dn3ss\framework\Entity;

/**
 * Class Option
 *
 * @inheritdoc
 * @package m4dn3ss\entities
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 *
 */
class Option extends Entity
{
    protected static $tableName = 'options';

}