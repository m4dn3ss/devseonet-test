<?php

namespace m4dn3ss\controllers;

use m4dn3ss\App;
use m4dn3ss\entities\OptionValue;
use m4dn3ss\entities\Product;
use m4dn3ss\framework\Controller;

class MainController extends Controller
{

    /**
     * @throws \Exception
     */
    public function index()
    {

        $filter = App::request()->getQuery('filter');

        $products = Product::filterProducts($filter);

        $productIds = array();
        foreach ($products as $product) {
            $productIds[] = $product['id'];
        }

        $preparedOptions = OptionValue::getOptions($productIds);

        $this->render('main', ['products' => $products, 'options' => $preparedOptions, 'filter' => $filter ?? []]);
    }
}