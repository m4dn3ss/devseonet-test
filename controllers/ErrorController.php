<?php

namespace m4dn3ss\controllers;

use m4dn3ss\framework\Controller;

class ErrorController extends Controller
{
    public function error404()
    {
        header("HTTP/1.0 404 Not Found");
        $this->render('error', ['code' => 404]);
    }
}