<?php
/** @var array $products */
/** @var array $options */
/** @var array $filter */
?>
<div class="row">
<div class="col-3">
    <?php if (isset($options) && !empty($options)):?>
        <form method="get" id="filter">
        <?php foreach ($options as $name => $optionValues): ?>
                <h3><?= $name ?></h3>
                <?php foreach ($optionValues as $value): ?>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="option-<?= $value['value_id'] ?>" name="filter[]" value="<?= $value['value_id'] ?>" <?= (in_array($value['value_id'], $filter) ? 'checked' : '') ?>>
                        <label class="form-check-label" for="option-<?= $value['value_id'] ?>"><?= $value['name'] ?> <span class="text-muted">(<?= $value['products_count'] ?>)</span></label>
                    </div>
                <?php endforeach; ?>
        <?php endforeach; ?>
        </form>
    <?php else: ?>
        <p class="alert alert-warning">No options found</p>
    <?php endif;?>
</div>
<div class="col-8">
    <?php if (isset($products) && !empty($products)):?>
        <?php foreach ($products as $product): ?>
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title"><?= $product['name'] ?></h5>
                    <p class="card-text"><?= $product['description'] ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <p class="alert alert-warning">No products found</p>
    <?php endif;?>
</div>
</div>