<!DOCTYPE html>

<html>
<head>
    <title>Welcome Page</title>

    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/style.css"></head>
<body>
<div class="container">
    <header>
        <h1>Shop</h1>
        <hr>
    </header>
    <?php include($viewContent) ?>
</div>
<script src="/assets/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/main.js"></script>
</body>
</html>